---
hide:
  - navigation
  - site_name
  - site_url
  - toc
  - site_nav
---

Vous pouvez retrouver mes projets sur [:fontawesome-brands-github: GitHub](http://github.com/Hmarga)
et [:fontawesome-brands-gitlab: GitLab](https://gitlab.com/Hmarga).

## __[Analyse de l'emploi dans le secteur de la Data.](https://gitlab.com/Hmarga/emploi_secteur_data)__

> Février 2021

Récupération automatique et analyse des offres d'emploi dans le secteur de la Data.

Projet realisé en equipe, en suivant les principes des méthodes **Agiles**, et basé sur l'extraction de données de trois differents sites d'emploi à travers de leurs propes **APIs** ou **scraping**. 

Il est montré un développement web dans un format de base de données téléchargeable, cartographie vers **Folium** et graphiques de tendances d'emploi.

Les objectifs principaux étaient de:

<ul>
  <li> Generer une base de données propre et téléchargeable avec les intitulés de postes, date de publication, nom de l'entreprise, lieu, classification par type de poste et compétences clés; sur la région Auvergne-Rhône-Alpes.</li> 
  <li> Montrer graphiquement l'historique de la proportion de postes par type, pendant une ou plusieures semaines.</li> 
</ul>


| Tableau | Carto |
|:---:|:---:|
|![HpP](static/tableau_offres.png)|![HpP](static/carto_offres.png)|
|[Lien au tableau](./static/tableau_offres.png)|[Lien à carto](./static/carto_offres.png)|


Pour connaître plus de details sur ce projet, cliquez sur les liens suivant:


* [Pipeline](https://gitlab.com/Hmarga/emploi_secteur_data/-/blob/master/images/Pipeline.png) 
* [README](https://gitlab.com/Hmarga/emploi_secteur_data/-/blob/master/README.md)

Compétences : acquisition, importation, extraction, analyse et dashboard vers développement web.





## __[Permaculture et Terres Arables.](https://github.com/Hmarga/Lands)__

> Julliet 2020 - Septembre 2020

Estimer la superficie des terres arables de chaque pays en fonction de sa population (Hectares par Famille - HpF); en utilisant comme base l'estimation de la superficie des terres dont une personne a besoin selon le concept de la permaculture.

Les objectifs principaux étaient de:
<ul>
  <li> Montrer la répartition des terres arables (Cultivables Lands en anglais) par pays, dont les sources proviennent des bases de données des Nations Unies; en utilisant les données de latitudes et longitudes venant de la bibliothèque <b>geopandas</b>. </li>
  <li> Utiliser les bibliothèques <b>Python</b> pour gérer / former / extraire / commander les données correspondantes utilisées pour cette étude. </li>
</ul>

![HpP](./static/ha_ref_w.png)

Pour connaître plus de details sur ce projet, cliquez sur les liens suivant:

* [Abstract](https://github.com/Hmarga/Lands/blob/master/Abstract.md) 
* [Schéme](https://github.com/Hmarga/Lands/blob/master/images/Scheme_lands_.png) 
* [README](https://github.com/Hmarga/Lands/blob/master/README.md)

_Compétences_: acquisition, importation, extraction, analyse et visualisation de données.

_Outils Techniques_ : déploiement en GitLab Pages, Data visualisation **Seaborn** et **Folium**, bibliothéques pour scraping: **request** et **scrapy**.
