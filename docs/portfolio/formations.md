---
hide:
  - navigation: ""
  - site_name: ""
  - site_url: ""
  - toc : ""
  - site_nav: ""
---

### [Simplon Développeur.se Data](https://simplon.co/formation/developpeur-data/9)

> Novembre 2020 - Septembre 2021, Grenoble

Formation de Data Developer, dont le but est d’apprendre le cycle de vie des données numériques : depuis son extraction et stockage en base de données jusqu'à la livraison de données utilisables, via l’interprétation visuelle ou un support adapté pour un usage tiers. Tout en garantissant la cohérence des données avant et après traitement.

Avec l'aide d'une équipe de formateurs professionnels, on apprend et utilise la méthodologie Agile pour réaliser des projets, de façon individuelle ou en groupe, en présentiel ou à distance, avec une pédagogie active et intensive.

Le métier de développeur·se data s’articule alors autour de 2 activités principales :

- Développer une base de données
- Exploiter une base de données

### [Path Data Scientist in Python de Dataquest.io](https://www.dataquest.io/)

> Janvier 2019 - En cours, À distance.

Formation en ligne, pour apprendre les principales bibliothèques et méthodes de Python pour la Data Science.

[Certificats obtenus](https://app.dataquest.io/profile/helenadiaz)

### [Ingénieur électronique Université Simón Bolívar](http://www.usb.ve/)

> Sep 1999 – Juin 2005, Caracas – Venezuela.

Bac + 5 dans une académie d'enseignement supérieur.

