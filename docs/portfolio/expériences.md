---
hide:
  - navigation
  - site_name
  - site_url
  - toc
  - site_nav
---

#### [Télécharger mon CV](./static/CV_Helena_Diaz.pdf)

### [Ingénieure Développement à Fusion (ODOO - open source)](https://www.fusion.com.ar/)
> Nov. 2016 – Avr. 2018, Buenos Aires- Argentina

<ul>
  <li> Analyser la faisabilité technique des demandes des clients pour la mise en pratique. </li>
  <li> Intégration et migration de bases de données.</li>
  <li> Modifier ou concevoir des nouvelles idées pour adapter l’ERP aux besoins du client.</li>
  <li> Installation et adéquation des modules pertinents, dans le code HTML/CSS ou Python propriétaire.</li>
  <li> Superviser l’installation et le test des modifications apportées. </li>
</ul>

### [Consultante de modélisation](http://www.movilnet.com.ve/sitio/)

> Juin 2012 – Mars 2014, Caracas - Venezuela.

<ul>      
  <li> Conception et construction des modèles de comportement des serveurs du réseau mobile à travers d’algorithmes mathématiques, en fonction des spécifications techniques d’origine (capacité de traitement de données, stockage, KPIs propes du serveur) et de l’estimation des futurs besoins de trafic et des nouveaux clients. </li>
<li> Calcul de futurs besoins de ressources physiques (hardware) en raison du modèle. </li>
<li> Réalisation des documents avec les résultats calculés précédemments à fin d’être utilisé dans les prochaines étapes du processus de planification de la totalité du réseau mobile. </li>
</ul>

### [Ingénieure Optimisation du Core Network](http://www.movilnet.com.ve/sitio/)

> Oct 2005 – Juin 2012, Caracas - Venezuela
<ul>      
  <li> Évaluation des KPIs de Core Network (réseau central) et des liens de signalisation, et gestion des schémas de routage.</li> 
  <li> Suggestion de changements de configuration des équipes à fin d'améliorér la performance du réseau. </li> 
  <li> Consultation de base de données PostgreSQL pour obtenir des statistiques et construire ou évaluer les KPIs de chaque serveur.</li>
</ul>
