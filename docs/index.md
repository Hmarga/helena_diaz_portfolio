---
hide:
  - navigation
  - site_name
  - site_url
  - toc
  - site_nav
---

![greywawe](portfolio/static/greywawe.png){: style="width: 150%", "height: 3%"}

# Helena Díaz


> Développeuse Data

En formation de développeuse Data Junior à Simplon.co depuis novembre 2020 et d’une durée de 9 mois, je recherche un stage du 4 juin au 30 juillet 2021.

Ayant pour objectif d’évoluer professionnellement, je suis motivée d'apprendre, de comprendre et de contribuer à des projets liés au domaine de la data. 

J’ai une forte capacité d’adaptation, d’organisation et de persévérance.

Attirée par les activités de plein air, j'aime bien faire des randonnées en montagne et du vélo.

---

[:fontawesome-brands-linkedin:{: style="transform: scale(1.5); margin: 0 0.5rem;" }](https://Linkedin.com/in/helenamdiazg "LikedIn")
[:fontawesome-brands-gitlab:{: style="transform: scale(1.5); margin: 0 0.5rem;" }](https://gitlab.com/Hmarga "GitLab")
[:fontawesome-brands-github:{: style="transform: scale(1.5); margin: 0 0.5rem;" }](http://github.com/Hmarga "GitHub")

